#!/usr/bin/perl

use strict ;
use warnings ;
use IO::File ;
use XML::LibXML ;
use Data::Dumper ;
use constant 'DEFAULT_NS' => 'xhtml' ;

BEGIN {
	%::NS = (
		+ __PACKAGE__->DEFAULT_NS( ) => 'http://www.w3.org/1999/xhtml' ,
		'xi'	=> 'http://www.w3.org/2001/XInclude' ,
	) ;
}

sub css( $;\% ) {
	my $attr = &attr( shift( @_ ) ) ;
	my $result = shift( @_ ) || { } ;
	foreach my $ns ( keys( %$attr ) ) {
		foreach my $key ( keys( %{ $attr->{ $ns } } ) ) {
			$result->{ $key } = $attr->{ $ns }{ $key } ;
		}
	}
	return $result ;
}
sub attr( $ ) {
	my ( $str , %result ) = shift( @_ ) ;
	while (
		defined( $str ) &&
		$str =~ s{
			([\@]?)(?:
				(\w[\w\-]*\w|\w)
				(?:\|(\w[\w\-]*\w|\w))?
			)\s*([\=\:])\s*(?:([\'\"\`])(.*?)\5|(\S+?)(?:;|\s|$))
		}{}xs
	) {
		my( $value , $name , $ns ) ;
		# die( qq(Sintax error near "\Q$&\E") ) if defined( $1 ) && length( $1 ) && ( $4 ne '=' ) ;
		if( defined( $6 ) ) {
			$value = $6 ;
		} else {
			$value = $7 ;
		}
		if( defined( $2 ) ) {
			if( defined( $3 ) ) {
				$ns = $2 ;
				$name = $3 ;
			} else {
				$ns = &DEFAULT_NS( ) ;
				$name = $2 ;
			}
		} else {
			die( 'Attribute name is not defined' ) ;
		}
		$result{ $ns } ||= { } ;
		if( $name eq 'class' ) {
			$result{ $ns }{ $name } = [ split( /\s+/ , $value ) ] ;
		} else {
			$result{ $ns }{ $name } = $value ;
		}
	}
	return \%result ;
}
sub info( $ ) {
	my $str = shift( @_ ) ;
	my ( $name , $ns , @result ) ;
	$str =~ s{^[\s\,]*(?:
		(\w[\w\-]*\w|\w)
		(?:\|(\w[\w\-]*\w|\w))?
	)?(\[\s*(.*?)\s*\])?}{}xs ;
	my $attr = &attr( $3 ) ;
	if( defined( $1 ) ) {
		if( defined( $2 ) ) {
			$ns = $1 ;
			$name = $2 ;
		} else {
			$ns = &DEFAULT_NS( ) ;
			$name = $1 ;
		}
	} else {
		$ns = &DEFAULT_NS( ) ;
		$name = 'div' ;
	}
	while ( $str =~ s{([\.\:\#])(\w[\w+\-]*\w|\w)}{}xs ) {
		if ( $1 eq '#' ) {
			$attr->{ &DEFAULT_NS( ) } ||= { } ;
			$attr->{ &DEFAULT_NS( ) }{ 'id' } = $2 ;
		} elsif ( $1 eq '.' ) {
			$attr->{ &DEFAULT_NS( ) } ||= { } ;
			$attr->{ &DEFAULT_NS( ) }{ 'class' } ||= [ ] ;
			push( @{ $attr->{ &DEFAULT_NS( ) }{ 'class' } } , $2 ) ;
		} else {
			push( @result , {
				'modifier'	=> $1 ,
				'name'		=> $2 ,
			} ) ;
		}
	}
	return {
		'ns'		=> $ns ,
		'name'		=> $name ,
		'attr'		=> $attr ,
		'modifiers'	=> \@result ,
		'value'		=> '' ,
	} ;
}
sub parse( $ ) {
	my( $str , @result ) = shift( @_ ) ;
	my $buff = '' ;
	my $item ;
	$str =~ s{/\*.*?\*/}{}gos ;
	$str =~ s{//.*/}{}gom ;
	while(
		defined( $str ) &&
		$str =~ m{.}gcsx
	) {
		if( $& eq '(' ) {
			my ( $result , $len ) = &parse( $' ) ;
			$item = {
				'info'		=> &info( $buff ) ,
				'children'	=> $result ,
			} ;
			pos( $str ) += $len ;
		} elsif( $& eq ')' ) {
			last( ) ;
		} elsif( $& eq '"' ) {
			my $pos = index( $' , '"' ) ;
			die( 'Non balanced quotas' ) if $pos == -1 ;
			my $delta = pos( $str ) - $pos ;
			my $value = substr( $' , 0 , $pos ) ;
			pos( $str ) += ++ $pos ;
			$item = {
				'info' => {
					'name'	=> ':text' ,
					'value' => $value ,
				}
			} ;
		} elsif( $& eq '[' ) {
			my $pos = index( $' , ']' ) ;
			die( 'Non balanced brackets' ) unless ++ $pos ;
			$buff .= $& . substr( $' , 0 , $pos ) ;
			pos( $str ) += $pos ;
			next( ) ;
		} elsif( grep( $& eq $_ , ',' , ';' , ' ' , "\t" , "\r" , "\n" ) ) {
			$item = {
				'info' => &info( $buff ) ,
			} if $buff =~ m{\S} ;
		} else {
			$buff .= $& ;
			next( ) ;
		}

		if( defined( $item ) ) {
			push( @result , $item ) ;
			my $found = 0 ;
			my $substr = \substr( $str , pos( $str ) ) ;
			my $css = { } ;
			while ( $$substr =~ m{^
				\s*\{
					\s*(.*?)\s*
				\}
			}gcsx ) {
				&css( $1 , $css ) ;
				pos( $str ) += pos( $$substr ) ;
			}
			$item->{ 'info' }{ 'attr' }{ &DEFAULT_NS( ) }{ 'style' } = $css if %$css ;
			undef( $item ) ;
		}
		$buff = '' ;
	}
	return ( \@result , pos( $str ) ) ;
}

sub toXML( $ ) {
	my( $struct , $second ) = @_ ;
	my $result = '' ;
	foreach my $item ( @$struct ) {
		if( $item->{ 'info' }{ 'name' } eq ':text' ) {
			$result .= $item->{ 'info' }{ 'value' } if exists( $item->{ 'info' }{ 'value' } ) ;
		} else {
			$result .= '<' ;
			$result .= $item->{ 'info' }{ 'ns' } . ':' unless $item->{ 'info' }{ 'ns' } eq &DEFAULT_NS( ) ;
			$result .= $item->{ 'info' }{ 'name' } ;
			unless( defined( $second ) ) {
				$second = 1 ;
				$result .= ' xmlns:' . $_ . '="' . $::NS{ $_ } . '"' foreach keys( %::NS ) ;
				$result .= ' xmlns="' . $::NS{ &DEFAULT_NS( ) } . '"' ;
			}
			if( exists( $item->{ 'info' }{ 'attr' } ) ) {
				while ( my ( $attrNS , $attrItem ) = each( %{ $item->{ 'info' }{ 'attr' } } ) ) {
					while( my ( $attrName , $attrValue ) = each( %{ $attrItem } ) ) {
						$result .= ' ' ;
						$result .= $attrNS . ':' unless $attrNS eq &DEFAULT_NS( ) ;
						$result .= $attrName . '="' ;
						if( ref( $attrValue ) eq 'ARRAY' ) {
							$result .= join( ' ' , @$attrValue ) ;
						} elsif( ref( $attrValue ) eq 'HASH' ) {
							$result .= join( ' ' , map{ $_ . ': ' . $attrValue->{ $_ } }sort( keys( %$attrValue ) ) ) ;
						} else {
							$result .= $attrValue ;
						}
						$result .= '"' ;
					}
				}
			}
			$result .= '>' .&toXML( $item->{ 'children' } , $second ) ;
			$result .= '</' ;
			$result .= $item->{ 'info' }{ 'ns' } . ':' unless $item->{ 'info' }{ 'ns' } eq &DEFAULT_NS( ) ;
			$result .= $item->{ 'info' }{ 'name' } . '>' ;
		}
	}
	return $result ;
}

undef( $/ ) ;

my( $result ) = &parse( <> ) ;
my $xml = &toXML( $result ) ;
my $h = XML::LibXML->new( ) ;
my $doc = $h->parse_string( $xml ) ;
$doc->toFH( \*STDOUT ) ;